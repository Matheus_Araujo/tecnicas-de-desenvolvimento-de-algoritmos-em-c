#include <stdio.h>

struct pessoa{
    char nome[100];
    int idade;
    struct endereco{
        char rua [100];
        char numero [10];
        double cep;
    };
    struct endereco endereco;

};

int main (){
    struct pessoa pessoa;

    printf("----------------------------------ficha da pessoa-----------------------------------\n");
    printf("Digite o nome : ");
    fgets(pessoa.nome,100,stdin);

    printf("Digite a idade : ");
    scanf("%d",&pessoa.idade);

    printf("Digite a rua : ");
    fflush(stdin);
    fgets(pessoa.endereco.rua,100,stdin);


    printf("Digite o numero : ");
    fgets(pessoa.endereco.numero,10,stdin);

    printf("Digite o CEP (apenas numeros) : ");
    scanf("%d",&pessoa.endereco.cep);

    printf("\n-------------Os dados coletaram foram :--------------\n");
    printf("Nome : %s",pessoa.nome);
    printf("Idade : %d",pessoa.idade);
    printf("\nRua : %s",pessoa.endereco.rua);
    printf("Numero : %s",pessoa.endereco.numero);
    printf("Cep : %d",pessoa.endereco.cep);




    return 0;
}
